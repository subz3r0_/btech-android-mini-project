package com.example.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

public class question_page extends AppCompatActivity{
    //declaring variable to start database
    DatabaseReference mRef;
    Button a,b,c,d;
    ArrayList<Quest> questions;

    TextView at,bt,ct,dt,qt,points;

    int num = 1;
    int n = 0;

    RelativeLayout ll;
    LinearLayout linearLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_page);

        //connected buttons to a,b,c and d
        a = findViewById(R.id.optiona);
        b = findViewById(R.id.optionb);
        c = findViewById(R.id.optionc);
        d = findViewById(R.id.optiond);

        //connected question and options to textview
        qt = findViewById(R.id.question);
        at = findViewById(R.id.option_a_text);
        bt = findViewById(R.id.option_b_text);
        ct = findViewById(R.id.option_c_text);
        dt = findViewById(R.id.option_d_text);

        ll = findViewById(R.id.quiz_ll);
        linearLayout = findViewById(R.id.point_ll);
        //connected points to text view
        points = findViewById(R.id.points_c);

        //made array to store questions
        questions = new ArrayList<>();

        mRef= FirebaseDatabase.getInstance().getReference("quiz");


        mRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Quest q = snapshot.getValue(Quest.class);
                    questions.add(q);
                }
                Collections.shuffle(questions);
                setData();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }


    void setData(){
        final Quest q = questions.get(n);

        qt.setText(q.getQuestionDescription());
        at.setText(q.getOptionA());
        bt.setText(q.getOptionB());
        ct.setText(q.getOptionC());
        dt.setText(q.getOptionD());

        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(q.getAnswer().equals("A")){
                    Toast.makeText(question_page.this, "Correct", Toast.LENGTH_SHORT).show();
                    num++;
                    points.setText("Points: "+(num-1));
                    n++;
                    setData();
                }
                else {
                    num--;
                    points.setText("Points: "+(num-1));
                    n++;
                    setData();
                    Toast.makeText(question_page.this, "Wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(q.getAnswer().equals("B")){
                    Toast.makeText(question_page.this, "Correct", Toast.LENGTH_SHORT).show();
                    num++;
                    points.setText("Points: "+(num-1));
                    n++;
                    setData();
                }
                else {
                    num--;
                    n++;
                    setData();
                    points.setText("Points: "+(num-1));
                    Toast.makeText(question_page.this, "Wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });
        c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(q.getAnswer().equals("C")){
                    Toast.makeText(question_page.this, "Correct", Toast.LENGTH_SHORT).show();
                    num++;
                    n++;
                    points.setText("Points: "+(num-1));
                    setData();
                }
                else {
                    num--;
                    n++;
                    setData();
                    points.setText("Points: "+(num-1));
                    Toast.makeText(question_page.this, "Wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });

        d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(q.getAnswer().equals("D")){
                    Toast.makeText(question_page.this, "Correct", Toast.LENGTH_SHORT).show();
                    num++;
                    n++;
                    points.setText("Points: "+(num-1));
                    setData();
                }
                else {
                    num--;
                    n++;
                    setData();
                    points.setText("Points: "+(num-1));
                    Toast.makeText(question_page.this, "Wrong", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


}
