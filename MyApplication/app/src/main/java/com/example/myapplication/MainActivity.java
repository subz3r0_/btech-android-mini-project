package com.example.myapplication;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    //make firebaseauth variables
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    //assigining RC_SIGN_IN variable
    public static final int RC_SIGN_IN = 1;
    //declare quiz button
    Button quizbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //initialize quiz button
        quizbtn=findViewById(R.id.startbutton);
        quizbtn.setOnClickListener(this);
        //initialise firebaseauth
        mFirebaseAuth = FirebaseAuth.getInstance();
        //initialise authstatelistener
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user!=null){
                    //user is signed in
                    //make a toast
                    Toast.makeText(MainActivity.this,"you're signed in",Toast.LENGTH_SHORT).show();

                }else{
                    //user is signed out
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setIsSmartLockEnabled(false)
                                    .setAvailableProviders(Arrays.asList(
                                            new AuthUI.IdpConfig.EmailBuilder().build(),
                                            new AuthUI.IdpConfig.PhoneBuilder().build(),
                                            new AuthUI.IdpConfig.GoogleBuilder().build()))
                                            new AuthUI.IdpConfig.FacebookBuilder().build()))
                                            //new AuthUI.IdpConfig.TwitterBuilder().build()))
                                    .build(),
                            RC_SIGN_IN);
                }


            }
        };

    }

    //implementation of signout menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    //implementation of signout method
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sign_out_menu:
                AuthUI.getInstance().signOut(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    //handling cancelled signin
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
                super.onActivityResult(requestCode, resultCode, data);
                if (requestCode == RC_SIGN_IN) {
                        if (resultCode == RESULT_OK) {
                                // Sign-in succeeded, set up the UI
                                        Toast.makeText(this, "Signed in", Toast.LENGTH_SHORT).show();
                            } else if (resultCode == RESULT_CANCELED) {
                                // Sign in was canceled by the user, finish the activity
                                        //Toast.makeText(this, "Signin canceled", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                    }
            }

    //on resuming add the state listener
    @Override
    protected void onResume() {
        super.onResume();
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }
    //on pausing remove the listener
    @Override
    protected void onPause() {
        super.onPause();
        if (mAuthStateListener != null) {
            mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
        }
    }

    @Override
    public void onClick(View view) {

        if(view==quizbtn){
            startActivity(new Intent(this,question_page.class));
        }

    }
}