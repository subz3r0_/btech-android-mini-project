package com.example.myapplication;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by vishnuprasad on 23/03/18.
 */

public class firebase extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
